# REIGN - APP:

Reign APP can GET and DELETE stories posts given by an external API


## Installation

### Reign-server and Reign-app:

in order to use reign-server and reign-app you must install all dependencies on each individual project

```bash
npm install
```


## Execution

### Reign-server and Reign-app:

in order to turn on reign-server and reign-app you must type on each individual project:

```bash
npm run start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)