export interface Hit {
    author: string;
    created_at: string;
    created_at_i: number;
    deletedAt: string;
    isDeleted: boolean;
    objectID: number;
    story_id: number;
    story_title: string;
    story_url: string;
    title: string;
    url: null;
    __v: number;
    _id: string;
}
