import { useEffect, useState } from 'react'
import { Hit } from '../../interfaces/stories';
import { Story } from '../Story/Story';
import Axios from 'axios';

export const StoriesView = () => {


    const [stories, setStories] = useState<Hit[]>([])

    useEffect(function () {
        fetch('http://localhost:4000/stories', {
            method: 'GET',
            mode: 'cors',
        })
            .then(res => res.json())
            .then((response: Hit[]) => setStories(response))
    }, []);


    const handleClick = async (id: string) => {
        try {
            await Axios.delete(`http://localhost:4000/stories/${id}`);
            console.log(id);
            const newStories = stories.filter((story) => story._id !== id);
            setStories(newStories);
        } catch (err) {
            console.log(err);
        }
    };


    return (
        <div className="container">
            {
                stories.map(item => <Story key={item._id} story={item} delete={handleClick}></Story>)
            }
        </div>
    )
}
