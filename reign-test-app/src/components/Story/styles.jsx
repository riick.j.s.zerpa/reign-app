import styled from 'styled-components'


export const Anchor = styled.a`
text-decoration: none;
`

export const RowTable = styled.div`
font-family: "Helvetica", "Roboto", "SegoeUI";
justify-content: space-between;
display: flex;
border-bottom: 1px solid #ccc;
background-color: #fff;
&:hover {
    background-color: #fafafa;
}
`

export const LeftCol = styled.div`
display: flex;
margin-left: 10px;
`
export const Title = styled.div`
margin-top: 5px;
margin-bottom: 5px;
display: flex;
align-items: center;
`

export const TitleP = styled.p`
font-size: 13pt;
color: #333;
font-weight: bold;
`

export const Author = styled.div`
margin-left: 10px;
margin-top: 5px;
margin-bottom: 5px;
display: flex;
align-items: center;
`

export const AuthorP = styled.p`
font-size: 11pt;
color: #999;
font-weight: bold;
`

export const RightCol = styled.div`
margin-right: 100px;
display: flex;
align-items: center;
justify-content: space-between;
color: #333;
font-size: 13pt;
width: 150px;
`

export const DateP = styled.p`
font-size: 13pt;
color: #333;
font-weight: bold;
`

export const DeleteButton = styled.div`
display: none;

img {
  max-width: 24px;
}

${RowTable}:hover & {
    display: block;
  }
`