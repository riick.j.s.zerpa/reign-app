import moment from 'moment';
import trashIcon from '../../assets/icons/delete.png'
import { Author, AuthorP, RightCol, DateP, RowTable, Title, TitleP, LeftCol, DeleteButton, Anchor } from './styles'

export const Story = ({
    story: {
        author,
        created_at,
        story_title,
        story_url,
        title,
        url,
        _id },
    delete: handleClick,
}: any) => {

    let testClick = (e: any) => {
        e.stopPropagation()
        handleClick(_id)
    }

    let newDate = (date: string) => {
        const today = new Date();
        const Ddate = new Date(date);

        let difTime = today.getTime() - Ddate.getTime()
        let difDays = Math.floor(difTime / (1000 * 3600 * 24));



        if (difDays == 0) {

            return moment(Ddate).format('LT');

        } else if (difDays == 1) {

            return "Yesterday";

        } else if (difDays >= 2) {
            return moment(Ddate).format('MMM DD');
        }

    }


    return (
        <div>
            {
                title !== null || story_title !== null
                &&
                <Anchor onClick={() => window.open(story_url ? story_url : url, '_blank')}>
                    <RowTable className="rowTable">
                        <LeftCol className="titles">
                            <Title className="title">
                                <TitleP>{story_title ? story_title : title}</TitleP>
                            </Title>
                            <Author className="author">
                                <AuthorP>- {author} -</AuthorP>
                            </Author>
                        </LeftCol>
                        <RightCol className="date">
                            <div className="creationDate">
                                <DateP>{newDate(created_at)}</DateP>
                            </div>
                            <DeleteButton className="deleteButton">
                                <img src={trashIcon} onClick={testClick} alt="" />
                            </DeleteButton>
                        </RightCol>
                    </RowTable>
                </Anchor>
            }
        </div>
    )
}
