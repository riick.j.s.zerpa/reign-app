import React from "react"
import { HeaderContainer, Phrase, Title } from "./styles"


export const Header = () => {
    return (
        <HeaderContainer>
            <Title className="">HN Feed</Title>
            <Phrase>We LOVE hacker news!</Phrase>
        </HeaderContainer>
    )
}