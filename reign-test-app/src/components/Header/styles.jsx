import styled from 'styled-components'


export const HeaderContainer = styled.div`
background-color:#333333;
height: 150px;
padding: 20px;
/* text-align: left; */
`
export const Title = styled.h1`
font-size: 4rem;
font-family: "Helvetica", "Roboto", "SegoeUI";
text-align:center;
margin-top: 0;
margin-bottom: 2rem;
color: #fff;
`
export const Phrase = styled.h3`
font-size: 1rem;
font-family: "Helvetica", "Roboto", "SegoeUI";
text-align:center;
color: #fff;
`

