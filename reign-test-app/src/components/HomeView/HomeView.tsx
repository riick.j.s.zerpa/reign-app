import React from "react"
import { Header } from "../Header/Header"
import { StoriesView } from "../StoriesView/storiesView"


export const HomeView = () => {
    return (
        <div>
            <Header />
            <StoriesView />
        </div>
    )
}
