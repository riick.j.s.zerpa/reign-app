import { HttpModule, Module } from '@nestjs/common';
import { StoriesController } from './controllers/stories/stories.controller';
import { StoriesService } from './services/stories/stories.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StorySchema, Story } from './schemas/storiesSchema';
import { softDeletePlugin } from 'soft-delete-plugin-mongoose';
import { ScheduleModule } from '@nestjs/schedule';



@Module({
  imports: [HttpModule, ScheduleModule.forRoot(), MongooseModule.forFeature([{ name: Story.name, schema: StorySchema }]), MongooseModule.forRoot('mongodb+srv://dbUser:dbUserPassword@reingdb.7mzov.mongodb.net/reign-data', {
    connectionFactory: (connection) => {
      connection.plugin(softDeletePlugin);
      return connection;
    }
  }),],
  controllers: [StoriesController],
  providers: [StoriesService],
})
export class AppModule { }

