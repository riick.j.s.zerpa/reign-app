import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


export type StoryDocument = Story & Document;

@Schema()
export class Story {

    @Prop()
    created_at: string;

    @Prop()
    title: string;

    @Prop()
    url: string;

    @Prop()
    author: string;

    @Prop()
    story_id: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    @Prop()
    created_at_i: number;

    @Prop()
    objectID: number;
}


export const StorySchema = SchemaFactory.createForClass(Story);