import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { StoriesService } from '../../services/stories/stories.service';
import { StoriesController } from './stories.controller';

describe('StoriesController', () => {
  let controller: StoriesController;
  let service: StoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StoriesController],
      providers: [StoriesService, HttpService],
      imports: [AppModule]
    }).compile();

    controller = module.get<StoriesController>(StoriesController);
    service = module.get<StoriesService>(StoriesService);
  });


  describe('findAll', () => {
    it('should return an array of stories', async () => {
      const result: any = [{
        author: "spiorf",
        created_at: "2021-06-06T16:54:42.000Z",
        created_at_i: 1622998482,
        deletedAt: null,
        isDeleted: false,
        objectID: 27414418,
        story_id: 27408683,
        story_title: "El Salvador to adopt Bitcoin as legal tender",
        story_url: "https://www.cnbc.com/2021/06/05/el-salvador-becomes-the-first-country-to-adopt-bitcoin-as-legal-tender-.html",
        title: null,
        url: null,
        __v: 0,
        _id: "60bcff1163b4a2348cc4bb06"
      }];
      jest.spyOn(service, 'findInBD').mockImplementation(() => result);


      const testController = await controller.getDataBD()

      expect(testController.length).toBeGreaterThan(0);
    });
  });
});