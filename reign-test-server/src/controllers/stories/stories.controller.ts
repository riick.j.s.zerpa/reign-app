import { Controller, Delete, Get, Param } from '@nestjs/common';
import { StoriesService } from '../../services/stories/stories.service'

@Controller('stories')
export class StoriesController {

    constructor(private storiesService: StoriesService) { }

    @Get()
    async getDataBD(): Promise<any> {
        return this.storiesService.findInBD();
    }

    @Delete(':storyId')
    async deleDataBD(@Param('storyId') storyId: string): Promise<any> {
        return this.storiesService.removeStory(storyId);
    }
}
