import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Story, StoryDocument } from '../../schemas/storiesSchema';



@Injectable()
export class StoriesService {
    constructor(private httpService: HttpService, @InjectModel(Story.name) private readonly storyModel: Model<StoryDocument>,) { }


    @Cron(CronExpression.EVERY_HOUR)
    async findAll() {
        const response = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();
        this.createStory(response.data.hits);

        let nPages = response.data.nbPages

        for (let i = 2; i <= nPages; i++) {
            let newPages = await this.httpService.get(`https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=${i}`).toPromise();
            this.createStory(newPages.data.hits);
        }
    }


    saveStory(items: any[]) {

        items.forEach(async story => {
            let storyById = await this.storyModel.findOne({ story_id: story.story_id }).exec();
            if (storyById == null) {
                let createdStory = new this.storyModel(story)
                createdStory.save()
            }
        });
    }

    createStory(data: any[]) {
        this.saveStory(data)
    }

    async findInBD() {
        let dataBD = await this.storyModel.find({ isDeleted: false }).sort({ created_at: -1 }).exec();
        return dataBD
    }

    async removeStory(id) {

        const today = new Date();
        await this.storyModel.updateOne({ _id: id }, { isDeleted: true, deletedAt: today }).exec();

        this.findInBD();
    }
}
